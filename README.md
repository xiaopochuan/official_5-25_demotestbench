# Official_5-25_DEMOTestBench

## What is this repository for? ##

This is a Unity based 2d platform jumping game, the main purpose of this project is to help members familiar with the full process of game production and teamwork

### Contributor ###

* Chief Programmer @Mike Von
* Second Programmer @妹儿
* Game Designer @Victor Liu 
* Game Script Writer @PikaQui 

### Documents  ###

[Tencent Doc link (Is In CN)](
https://docs.qq.com/doc/DQXhFeGdRWkpQaldR)
### Games Current process and Conncepts ###
![](img/demo.gif)
![](img/concept1_crop.jpg)
![](img/concept2_crop.jpg)
![](img/concept3_crop.jpg)
![](img/concept4_crop.jpg)
![](img/concept5_crop.jpg)
![](img/concept6_crop.jpg)