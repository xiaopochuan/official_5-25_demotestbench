using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pressurePlate : MonoBehaviour
{

    private Animator door;
    private Animator _pressurePlate;
    private float timer;

    private void Awake()
    {
        door = door.GetComponent<Animator>();
        _pressurePlate = _pressurePlate.GetComponent<Animator>();
    }


    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.GetComponent<HeroController>()!= null)
        {
            door.SetBool("open", true);
            Debug.Log("Opening.");
        }
    }
}
