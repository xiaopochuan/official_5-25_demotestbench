using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingGround : MonoBehaviour
{
    public GameObject ground;
    public GameObject button;
    public GameObject groundInitPosREF;
    public MovingGround_SO baseData;

    [Header("电梯基本属性")]
    public Vector3 finalPos;
    public float movingTime, waitTime;
    private Vector3 buttonLocalPos;
    private bool isRunning;
    private float moveGroundID{get{return button.GetInstanceID();}}

    private void Start() 
    {
        buttonLocalPos = button.transform.localPosition;
        isRunning = false;

        // ground.GetComponent<SpriteRenderer>().sprite = baseData.groundSprite;
        // ground.GetComponent<SpriteRenderer>().color = baseData.groundColor;
        // button.GetComponent<SpriteRenderer>().sprite = baseData.buttonSprite;
        // button.GetComponent<SpriteRenderer>().color = baseData.buttonColor;

        EventSystem.instance.movingGroundButtonPressed += ButtonPressHandler;
    }
    private void OnDrawGizmosSelected() 
    {
        Vector3 initPosition = groundInitPosREF.transform.position;
        Gizmos.DrawLine(initPosition,this.transform.position + finalPos);
        Gizmos.DrawWireCube(this.transform.position + finalPos, ground.transform.localScale);
    }

    private void ButtonPressHandler(GameObject _objIn)
    {
        if (_objIn.GetInstanceID() != moveGroundID) return;

        if (!isRunning)
        {
            isRunning = true;
            //踩上去按钮,按钮向下移动
            Debug.Log(baseData.str_OnButtonPressed);
            LeanTween.moveLocal(button,buttonLocalPos + new Vector3(0, -buttonLocalPos.y, 0),0.5f);
            Invoke("MovingGroundAnim",0.6f);
            // Debug.Log(cumulateTime);
        }
    }

    private void MovingGroundAnim()
    {
        button.SetActive(false); //关闭显示button
        LeanTween.move(ground, groundInitPosREF.transform.position + finalPos, movingTime);
        Invoke("ResetBackPosition", movingTime + waitTime);
        // Debug.Log(cumulateTime);
    }

    private void ResetBackPosition()
    {
        LeanTween.move(ground, groundInitPosREF.transform.position, movingTime);
        Invoke("ResetButtonPos",movingTime);
        // Debug.Log(cumulateTime);
    }

    private void ResetButtonPos()
    {
        button.SetActive(true); //重新开启显示button
        LeanTween.moveLocal(button,buttonLocalPos,0.5f);
        isRunning = false;
    }
}
