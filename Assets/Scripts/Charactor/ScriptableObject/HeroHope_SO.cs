﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Hero Hope Data", menuName = "Character Status / Hero Hope Data")]
/// <summary>
/// 用于储存Hero角色的希望值相关的所有数据的ScriptableObject
/// </summary>
public class HeroHope_SO : ScriptableObject
{
    [Header("希望值恢复相关")]
    [Tooltip("Hero每秒MP恢复的点数（注意非MP上限）")] public float recoverMPBasicPerSec;
    [Tooltip("Hero每秒ST恢复的点数")] public float recoverSTBasicPersec;
    [Tooltip("Hero静止状态下MP恢复速率（1代表100%)")] public float idleMPRecoverSpeed;
    [Tooltip("Hero移动状态下MP恢复速率（1代表100%)")] public float moveMPRecoverSpeed;
    [Tooltip("Hero静止状态下ST恢复速率（1代表100%)")] public float idleSTRecoverSpeed;
    [Tooltip("Hero移动状态下ST恢复速率（1代表100%)")] public float moveSTRecoverSpeed;

    [Header("希望值消耗相关")]
    [Tooltip("每秒Hero跑步所消耗的ST")] public float runCostST;
    [Tooltip("每次Hero跳跃所消耗的ST")] public float jumpCostST;
    [Tooltip("每次Hero冲刺所消耗的ST")] public float dashCostST;
    [Tooltip("每次Hero开灯所消耗的MP")] public float lampCostMP;
    [Tooltip("Hero开灯后每秒消耗的MP")] public float lampOnMPCostPerSec;
    [Tooltip("Hero开灯情况下每次Drop的MP上限的点数")] public float maxMPDrop;
    [Tooltip("Hero开灯情况下多少秒Drop一次MP上限")] public float secMaxMPDrop;
    [Tooltip("Hero最低的MP上限与最大MP的百分比（最大为1）")] public float lowestMaxMPFloorInPercent;

}
