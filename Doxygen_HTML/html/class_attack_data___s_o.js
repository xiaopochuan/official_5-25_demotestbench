var class_attack_data___s_o =
[
    [ "chaseDistance", "class_attack_data___s_o.html#a37ff11163cca410466c3b1c0b8208be1", null ],
    [ "closeAttackCD", "class_attack_data___s_o.html#acdec36b602f7be7cd97c564955c19561", null ],
    [ "closeAttackDistance", "class_attack_data___s_o.html#a4366f6c54c0abd3a9e7dabacbd164926", null ],
    [ "closeAttackPoint", "class_attack_data___s_o.html#a364fb9fbfa1eed83c7db0ee2cc342192", null ],
    [ "rangeAttackCD", "class_attack_data___s_o.html#a4bb3d4d3b1d93b872b77a5bc94ebb610", null ],
    [ "rangeAttackDistance", "class_attack_data___s_o.html#ae8d320e3d42ac82b706b02d6a38afc93", null ],
    [ "rangeAttackPoint", "class_attack_data___s_o.html#a3f3840bc7b27940aea3ed4ba087928d6", null ],
    [ "unNamed", "class_attack_data___s_o.html#aed6f8f8be309f0310b776b70abed0296", null ],
    [ "unNamed1", "class_attack_data___s_o.html#aa09f075980abba48cb576a68cab72cff", null ],
    [ "unNamed2", "class_attack_data___s_o.html#a75bbce63e8ef8c9f954c6256752259ef", null ]
];