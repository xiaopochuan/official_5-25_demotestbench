var class_hero_controller =
[
    [ "ChangeLightHandler", "class_hero_controller.html#a40a6ef4e57215750edd453d491444379", null ],
    [ "CharacterControl", "class_hero_controller.html#a7613b0850bb0f1ed61a14d86a99d0f0f", null ],
    [ "Ground", "class_hero_controller.html#a6cd2582172972da4578df368423ed553", null ],
    [ "GroundCheck", "class_hero_controller.html#af8e61d22139217b01e28f9430c04b8b3", null ],
    [ "tr", "class_hero_controller.html#af9fc3072bb2fdbe87d04b83008a88405", null ],
    [ "anim", "class_hero_controller.html#ad22b728494ff9ac23faaa0e9e76ecb3d", null ],
    [ "dashCostMP", "class_hero_controller.html#ab0d92121c18cce4f9e9cb07ff0695682", null ],
    [ "direction", "class_hero_controller.html#a5da05cfcec83d4d4ce40a9b929b544e8", null ],
    [ "jumpCostMP", "class_hero_controller.html#a2f0e6fdbc880007f12a7533de48f5c35", null ],
    [ "lampOnCostMP", "class_hero_controller.html#ae59574dd6c96fe5e7dceb708433e9bad", null ],
    [ "rb", "class_hero_controller.html#ab84c97cff8fc3eb90964cc337d21af42", null ]
];