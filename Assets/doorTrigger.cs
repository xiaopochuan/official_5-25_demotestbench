using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doorTrigger : MonoBehaviour
{
    private Animator _animator;
    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {   
        if (collision.tag == "Player")
        {
            _animator.SetBool("open", true);
            Debug.Log("Opening.");
        }
        
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
