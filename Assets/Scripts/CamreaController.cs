﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// [已弃用] 用于控制相机移动的基础脚本
/// </summary>
public class CamreaController : MonoBehaviour
{
    [SerializeField]public Transform player; // 主角位置
    public float speed = 5f; // 相机速度   
    [SerializeField]public float yOffset;
    Vector3 distance; // 主角和摄像机之间的距离                   


    void Start()
    {
        this.transform.position = new Vector3(player.position.x, player.position.y + yOffset, player.position.x);
        // 计算人物与摄像机之间的向量
        // 用当前摄像机的坐标 - 玩家的坐标（可以画一张图来算一算）
        distance = transform.position - player.position;
    }


    void FixedUpdate()
    {
        // 摄像机应该在的位置
        // 不直接赋值给当前摄像机的原因是，需要这个参数来实现一个延迟功能
        Vector3 targetCamPos = player.position + distance;

        // 给摄像机移动到应该在的位置的过程中加上延迟效果
        transform.position = Vector3.Lerp(transform.position, targetCamPos, speed * Time.deltaTime);
    }

}
