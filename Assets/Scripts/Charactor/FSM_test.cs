using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

/// <summary>
/// 是作为所有需要状态机调用的物品的父类来调用的FSM
/// </summary>
public class FSM_test : MonoBehaviour
{
public enum State {Idle, Walk, Attack, Damage, Dead, Sleep, Awake} //FIXME: //!这一块以后要加新的state只能到baseFSM里面来，因为ENUM无法Override，看有没有更好的方法
public State currentState; //* 当前的状态
public GameObject hero{ get{ return (GameObject.FindWithTag("Player"));} private set{}} //* 获取player
public float heroDistance;
public float heroYDiffrence;
public Animator anim{get{return(this.GetComponent<Animator>());}}
public virtual void Start() 
{
    currentState = State.Idle; //Note: 默认状态下所有敌人都从Idle开始调用, 收希望值影响的敌人要注意调用sleep
    heroDistance = Vector3.Distance(hero.transform.position, this.transform.position);
    //默认的事件订阅列表
    EventSystem.instance.heroBeenHit += HitOnHero;
}

public virtual void Update() 
{
    heroDistance = Vector3.Distance(hero.transform.position, this.transform.position);
    heroYDiffrence = hero.transform.position.y - this.transform.position.y;
    StateSwitch();
}

/// <summary>
/// 根据不同的FSM拓展Update函数和状态
/// </summary>
public virtual void StateSwitch()
{
    switch (currentState)
    {
        case State.Dead:
            StateDead();
            break;
        case State.Damage:
            StateDamage();
            break;
        case State.Attack:
            StateAttack();
            break;
        case State.Walk:
            StateWalk();
            break;
        case State.Idle:
            StateIdle();
            break;
        case State.Sleep:
            StateSleep();
            break;
        default:
            Debug.LogError("Fail to change state");
            break;
    }
}

/// <summary>
/// 发生State转换的时候调用的函数
/// </summary>
/// <param name="newState">传入的新的State</param>
public void ChangeState(State newState) //TODO: 这里考虑以后用泛型的方式传入一个子类的enum（储存state），然后更具子类的enum来change state
{
    if(currentState == newState) return;
    PlayAnimation(newState);
    currentState = newState;
}

    /// <summary>
    /// 传入State然后播放animation
    /// 一般在 <see cref="FSM_test.ChangeState(State newState)"/> 里调用
    /// </summary>
    /// <param name="animationName">State类</param>
    public void PlayAnimation(State animationName) => anim.Play(animationName.ToString());

    #region Event reacting functions
    /// <summary>
    /// 需要重载@Override, 默认订阅到heroBeenHit事件，处理敌人攻击到hero后的数值反应
    /// </summary>
    public virtual void HitOnHero() => throw new NotImplementedException();
    /// <summary>
    /// 需要重载@Override, 关于休眠状态所执行的function
    /// </summary>
    public virtual void StateSleep() => throw new NotImplementedException();
    /// <summary>
    /// 需要重载@Override, 关于死亡状态所执行的function
    /// </summary>
    public virtual void StateDead() => throw new NotImplementedException();
    /// <summary>
    /// 需要重载@Override, 关于收到伤害状态所执行的function
    /// </summary>
    public virtual void StateDamage() => throw new NotImplementedException();
    /// <summary>
    /// 需要重载@Override, 关于攻击Hero状态所执行的function
    /// </summary>
    public virtual void StateAttack() => throw new NotImplementedException();
    /// <summary>
    /// 需要重载@Override, 关于巡逻状态所执行的function
    /// </summary>
    public virtual void StateWalk() => throw new NotImplementedException();
    /// <summary>
    /// 需要重载@Override, 关于静止所执行的function
    /// </summary>
    public virtual void StateIdle() => throw new NotImplementedException();
    #endregion
}
