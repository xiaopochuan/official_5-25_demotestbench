﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Data", menuName = "Character Status / Basic Data")]
/// <summary>
/// 用于储存所有角色的基础属性的ScriptableObject
/// </summary>
public class CharactorData_SO:  ScriptableObject
{
    [Header("基础属性")]
    [Tooltip("角色最大HP值")] public float maxHP;
    [Tooltip("角色最大MP值")] public float maxMP;
    [Tooltip("角色目前HP值")] public float currentHP;
    [Tooltip("角色目前MP值")] public float currentMP;
    [Tooltip("角色走路速度")] public float walkSpeed;
    [Tooltip("角色跑步速度")] public float runSpeed;
    [Tooltip("角色跳跃力度")] public float jumpSpeed;
    [Tooltip("角色冲刺力度")] public float dashSpeed;
    [Tooltip("角色跳跃次数")] public int maxJumpAllowed;
    [Tooltip("(记录用)角色面朝方向")] public int direction;
    

    [Header("Hero 相关")]
    [Tooltip("角色目前ST值")] public float currentST; //ST值只会在hero调用所以放这了
    [Tooltip("Hero目前最大MP值")] public float currentMaxMP;
    [Tooltip("Hero目前最大HP值")] public float currentMaxHP;
    [Tooltip("角色冲刺冷却时间/s")] public float dashCountDown;
    [Tooltip("角色冲刺持续时间")] public float dashDuration;
    [Tooltip("角色跳跃与二段跳之间CD")] public float secondJumpCD;

    [Header("Enemy 相关")]
    [Tooltip("敌人跟踪Y轴的数值")] public float yDiffTrack;

    [Header("受希望值影响相关Enemy设定")]
    [Tooltip("多少MP使得Enmemy Awake")]public float e_awakeMP;
    [Tooltip("收到光照后每秒恢复多少MP")]public float e_recoverMP;
}
