using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CreateAssetMenu(fileName = "New FlotLamp Data", menuName = "Object Data/FlotLamp Data")]
/// <summary>
/// 用于储存所有有关FlotLamp相关的SO
/// </summary>
[System.Serializable]
public class FlotLamp_SO : ScriptableObject 
{
    [Header("FlotLamp 运动属性相关")]
    [Tooltip("FlotLamp的跟随速度，越小延迟越大")]public float followSpeed;
    [Tooltip("FlotLamp的开始换方向的时间")]public float dirTransTime;
    [Tooltip("FlotLamp简谐运动的调整, x 是振幅， y是频率")]public Vector2 yFloatSin;
    [Tooltip("两个数之间构成夹角确定单一方向上射灯的旋转范围,x 是上， y是下")]public Vector2 spotLightAngle;
    
    [Header("FlotLamp 光照相关属性相关")]
    [Tooltip("最大情况下light可以照射到的范围")]public float maxLightRange;

    [Header("FlotLamp 闪烁阶段List")]
    [SerializeField][Tooltip("按下 +element 增加新的stage")] public FlictStage[] flicStages;

}

[System.Serializable]
/// <summary>
/// 用于储存不同闪烁Stage的class， 闪代表一次闪烁，闪烁代表一组快速开关灯光的效果
/// </summary>
public class FlictStage 
{
    [SerializeField][Tooltip("阶段名称")]public string _stageName;
    [SerializeField][Tooltip("当前Stage所对应的范围")]public Vector2 _rangeMap;
    [SerializeField][Tooltip("下次组闪之间的间隔")]public float	 _flicGroupFreqInSec;
    [SerializeField][Tooltip("每组闪内有多少次闪烁")]public int _flicCountForAGroup;
    [SerializeField][Tooltip("每次闪烁关闭间隔")]public float _flicCloseDuration;
    [SerializeField][Tooltip("每次闪烁持续时长")]public float _flicDuration;
    [SerializeField][Tooltip("是否启用闪烁随机时间间隔")]public bool randActive;
    [SerializeField][Tooltip("闪烁随机时间间隔")]public Vector2 randRange;
    [HideInInspector] public int _Rand{get{return (randActive?1:0);}}
}
