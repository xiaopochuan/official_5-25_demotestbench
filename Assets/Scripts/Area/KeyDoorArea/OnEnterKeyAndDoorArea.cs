using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class OnEnterKeyAndDoorArea : MonoBehaviour
{
    private void Start() 
    {
        GetComponent<BoxCollider2D>().isTrigger = true;  
    }
    private void OnTriggerEnter2D(Collider2D other)     //默认情况下就是碰到Key就会call这个event
    {
        // Debug.Log(this.tag == "Move_key");
        if ((other.CompareTag("Player"))&& this.tag == "Pickable_obj")
        {
            EventSystem.instance.PickUp(this.gameObject);
        };
    }
    private void OnTriggerStay2D(Collider2D other) 
    {
        if (other.CompareTag("Player") && this.tag == "LockDoor")
        {
            EventSystem.instance.DoorKeyDetect(this.gameObject);
        }
    }

}
