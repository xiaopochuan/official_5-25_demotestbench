﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Attack Data", menuName = "Character Status / Attack Data")]
/// <summary>
/// 用于储存所有角色基础攻击的ScriptableObject
/// </summary>
public class AttackData_SO : ScriptableObject
{
    [Header("通用攻击属性")]
    
    [Tooltip("单次近距离攻击造成的伤害")]public float closeAttackPoint;
    [Tooltip("近距离攻击CD")]public float closeAttackCD;    
    [Tooltip("近距离攻击距离")]public float closeAttackDistance;    
    [Tooltip("单次远距离攻击造成的伤害")]public float rangeAttackPoint;
    [Tooltip("远距离攻击CD")]public float rangeAttackCD;
    [Tooltip("远距离攻击距离")]public float rangeAttackDistance;


    [Header("Hero 特有攻击属性")]
    public float unNamed;
    [Header("敌人攻击属性")]
    [Tooltip("开始追逐hero的距离")]public float chaseDistance;
    
    [Header("Vessel 敌人特有攻击属性")]
    public float unNamed1;
    [Header("Fly 敌人特有攻击属性")]
    public float unNamed2;
}
