
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Vessel敌人的行为逻辑和状态逻辑处理
/// </summary>
public  class VesselStatus : FSM_test
{
    public CharactorData_SO vesselBData;
    public AttackData_SO    vesselAData;
    public GameObject       wpObj; //武器的gameObject
    private Rigidbody2D rb{get{return GetComponent<Rigidbody2D>();}}
    private EdgeCollider2D wpRb{get{return wpObj.GetComponent<EdgeCollider2D>();}}
    private CircleCollider2D hopeCollide { get { return GetComponent<CircleCollider2D>(); } }

    private float centreShifDistance = 2.75f; //中心点移到物体中间所加的shift
    //* 基础属性赋值
    private float maxHP{get{return(vesselBData!=null?vesselBData.maxHP:0);}set{maxHP = value;}}
    private float maxMP{get{return(vesselBData!=null?vesselBData.maxMP:0);}set{maxMP = value;}}
    public float currentHP, currentMP;
    private float moveSpeed{get{return(vesselBData!=null?vesselBData.walkSpeed:0);}}
    private float jumpSpeed{get{return(vesselBData!=null?vesselBData.jumpSpeed:0);}}
    private float chaseDistance{get{return(vesselAData!=null?vesselAData.chaseDistance:0);}}
    private float awakeMP{get{return(vesselBData!=null?vesselBData.e_awakeMP:0);}}
    private float awakeRecovMp { get { return (vesselBData != null ? vesselBData.e_recoverMP : 0); } }
    private float yDiffChase { get { return (vesselBData != null ? vesselBData.yDiffTrack : 0); } }
    private Vector3 localScale;
    public bool isAlive, isAwake, isAwakeFinished,isChasing, isDamage, isAbleAttack, isAttacking;
    //* 攻击属性赋值
    private float closeAttackPoint{get{return(vesselAData!=null?vesselAData.closeAttackPoint:0);}}
    private float closeAttackCD{get{return(vesselAData!=null?vesselAData.closeAttackCD:0);}}
    private float closeAttackDistance{get{return(vesselAData!=null?vesselAData.closeAttackDistance:0);}}
    public float lastAttackTime;

    public override void Start()
    {
        base.Start();
        currentState = State.Sleep;
        PlayAnimation(State.Sleep);

        currentHP = maxHP;
        isAwakeFinished = false;
        localScale = this.transform.localScale;
    }

    public override void Update()
    {
        BoolUpdate();
        base.Update();
        lastAttackTime -= Time.deltaTime;

        //Debug 区域    
        // Debug.Log(isAttacking);
        
    }

    private void FixedUpdate() 
    {
        if (isChasing && isAwakeFinished && isAlive)
        {
            float direction = hero.transform.position.x - this.transform.position.x> 0 ? 1 : -1;
            transform.localScale = new Vector3(-localScale.x * direction, localScale.y, localScale.z);
            rb.transform.position += new Vector3(direction * vesselBData.walkSpeed * Time.deltaTime, 0f, 0f);
        }
    }

    /// <summary>
    /// 用于画出参考线的
    /// </summary>
    private void OnDrawGizmosSelected() 
    {
        //*画出追击距离的distance
        Gizmos.DrawWireSphere(this.transform.position, chaseDistance);
        //*画出近距离攻击判定的distance
        Gizmos.DrawWireSphere(this.transform.position, closeAttackDistance);
        //*画出追击y轴判断范围
        Gizmos.DrawWireCube(this.transform.position , new Vector3(5, yDiffChase));
    } 

    /// <summary>
    /// 处理所有在碰撞体内和碰撞相关的function
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerStay2D(Collider2D collision)
    {
        // Debug.Log(collision.name);
        if (collision.CompareTag("Player_lampLight") && !isAwake && !IsInvoking("RecoverMPBySec"))
        {
            //Debug.Log("I See You");
            Invoke("RecoverMPBySec", 1f);   
        }
    }
    /// <summary>
    /// 每隔一秒恢复MP值
    /// </summary>
    /// <returns>Yield 1s</returns>
    private void RecoverMPBySec()
    {
        currentMP += awakeRecovMp;
    }

    /// <summary>
    /// 全局所有的布尔值判断的更新
    /// </summary>
    private void BoolUpdate()
    {
        isAlive = currentHP >= 0 ? true : false;
        isAwake = currentMP >= awakeMP ? true : false;
        isChasing = heroDistance <= chaseDistance && heroDistance > closeAttackDistance && isAlive && heroYDiffrence <= yDiffChase ? true:false;
        isAbleAttack = heroDistance <= closeAttackDistance && lastAttackTime < 0 && isAlive && !isAttacking? true : false;
    }

    public override void HitOnHero()
    {
        if (isAttacking)
        {
            Debug.Log(String.Format("{0}对玩家造成了{1}点伤害","Vessel",closeAttackPoint));
            HeroStatus.instance.heroCurrentHP -= closeAttackPoint;
        }
    }

    #region State执行相关functions
    public override void StateIdle()
    {
        if (!isAlive)
        {
            ChangeState(State.Dead);
        }
        if (isDamage)
        {
            ChangeState(State.Damage);
        }
        if (isAbleAttack)
        {
            ChangeState(State.Attack);
            if (!IsInvoking("IsAttackingDelay")) Invoke("IsAttackingDelay", 0.25f); //延迟 0.25秒开始判定攻击
        }
        if (isChasing)
        {
            ChangeState(State.Walk);
        }
    }

    public override void StateAttack()
    {
        lastAttackTime = vesselAData.closeAttackCD; //攻击CD重置

        if (!isAlive)
        {
            ChangeState(State.Dead);
            isAttacking = false;
            return;
        }
        if (isChasing) 
        {
            ChangeState(State.Walk);
            isAttacking = false;
            return;
        }
        if (!IsInvoking("ChangeToIdle")) Invoke("ChangeToIdle", 1.2f);//延迟1.2进入Idle，等待攻击动画播完，重新判断是否还要攻击
    }

    public override void StateDamage()
    {
        //TODO: 修改相关属性
        if (!IsInvoking("ChangeToIdle")) Invoke("ChangeToIdle", 0.5f);//延迟.5进入Idle，等待受伤动画播完
    }

    public override void StateWalk()
    {
        if (!isAlive) 
        {
            ChangeState(State.Dead);
        }
        
        if (!isChasing) //前往idle state
        {
            if (!IsInvoking("ChangeToIdle")) Invoke("ChangeToIdle", 0f);
        }

        if (isAbleAttack)
        {
            ChangeState(State.Attack);
            if (!IsInvoking("IsAttackingDelay")) Invoke("IsAttackingDelay", 0.25f); //延迟 0.25秒开始判定攻击
        }
        
    }

    public override void StateDead()
    { 
        if (!IsInvoking("DestoryCurrentObject")) Invoke("DestoryCurrentObject", 0.2f); //0.2s后摧毁
    }

    public override void StateSleep()
    {
        if (!isAlive)
        {
            ChangeState(State.Dead);
        }
        if (isAwake)        //如果处于休眠状态
        {
            PlayAnimation(State.Awake);
            if (!IsInvoking("ChangeToIdle")) Invoke("ChangeToIdle", 4f);//防止多次引用invoking //延迟4s进入Idle，等待awake动画播完
        }
    }

    /// <summary>
    /// Invoke 函数调用，返回到Idle State， 用于等待播放动画
    /// </summary>
    private void ChangeToIdle()
    {
        isAttacking = false;
        isAwakeFinished = true;                 //FIXME： 不见得这种调用是好的
        ChangeState(State.Idle);
    }

    /// <summary>
    /// Invoke 函数调用， 用于处理延迟攻击动作动画的提前量
    /// </summary>
    private void IsAttackingDelay()                             //FIXME:以后需要在Attack data里来判定攻击的两个float 时间
    {
        if (currentState == State.Attack)
        {
            isAttacking = true;                 //如果0.25s之后还在Attack State，开始判定攻击
            Invoke("IsNotAttacking", 0.2f);     //0.2秒之后结束attack 判断，主要是从animation里得到的结果
        } 

    }

    /// <summary>
    /// Invoke 函数的调用，用于锁定attack时间在动画里的具体判定
    /// </summary>
    private void IsNotAttacking()
    {
        isAttacking = false;
    }

    /// <summary>
    /// Invoke 函数的调用，用于播放完死亡动画后再销毁物体
    /// </summary>
    private void DestoryCurrentObject()         //TODO: 未来如果要用对象池直接更改该function
    {
        Destroy(gameObject);
    }
    #endregion

}
