using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// 场景切换调用脚本
/// </summary>
public class SceneTransition : MonoBehaviour
{
    public void OnClickStartTheGame()
    {
        SceneManager.LoadScene("LightTestScene");
    }
}
