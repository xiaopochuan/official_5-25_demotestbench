﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

/// <summary>
/// 游戏运行时的主UI界面数据逻辑调用
/// </summary>
public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    [Header("Hero右上角属性相关")]

    [SerializeField] Slider HP_bar;
    [SerializeField] Slider MP_bar;
    [SerializeField] Slider MP_hitBar;
    [SerializeField] Text HP_Num, MP_Num;

    private float heroMaxHP, heroMaxMP, heroCurrentMP, heroCurrentMaxMP, heroCurrentHP, heroCurrentMaxHP;

    private void Start()
    {
        heroMaxHP = HeroStatus.instance.heroMaxHP;
        heroMaxMP = HeroStatus.instance.heroMaxMP;
    }
    private void Update()
    {
        UpdateHeroStatus();
        UpdateUI();
    }

    /// <summary>
    /// 更新UI上的数据
    /// </summary>
    private void UpdateUI()
    {
        //slider相关
        HP_bar.value = Mathf.InverseLerp(0, heroCurrentMaxHP, heroCurrentHP);
        MP_bar.value = Mathf.InverseLerp(0, heroMaxMP, heroCurrentMaxMP);
        MP_hitBar.value = Mathf.InverseLerp(0, heroMaxMP, heroCurrentMP);

        //text相关
        HP_Num.text = string.Format("{0}/{1}", (int)heroCurrentHP,(int) heroCurrentMaxHP);
        MP_Num.text = string.Format("{0}/{1}/{2}", (int)heroCurrentMP, (int)heroCurrentMaxMP, heroMaxMP);
    }

    /// <summary>
    /// 更新储存在ui本地的hero属性相关数据
    /// </summary>
    private void UpdateHeroStatus()
    {
        heroCurrentHP = HeroStatus.instance.heroCurrentHP;
        heroCurrentMaxHP = HeroStatus.instance.heroCurrentMaxHP;
        heroCurrentMP = HeroStatus.instance.heroCurrentMP;
        heroCurrentMaxMP = HeroStatus.instance.heroCurrentMaxMP;
    }
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            if (instance != null)
            {
                Destroy(gameObject);
            }
        }
        DontDestroyOnLoad(gameObject);
    }
}
