using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(CapsuleCollider2D))]
public class MovingGroundButtonCollide : MonoBehaviour
{
    private void Start() 
    {
        GetComponent<CapsuleCollider2D>().isTrigger = true;
    }
    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.CompareTag("Player"))
        {
            // Debug.Log(other.tag);
            EventSystem.instance.MovingGroundButtonPressed(this.gameObject);
        }
    }
}
