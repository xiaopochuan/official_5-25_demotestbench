﻿//namespace DentedPixel{

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
/// <summary>
/// 控制所有hero行为动作的脚本
/// </summary>
[RequireComponent(typeof(Rigidbody2D))]
public class HeroController : MonoBehaviour
{
        public Transform GroundCheck; //Check if the character collide with ground objects
        public LayerMask Ground; //!无法分开Ground和Environment layer里的其他tags，所有环境物品均可重置isGround变量
        // [SerializeField] GameObject lamp;
        [SerializeField] GameObject flotLampPointLight;
        [SerializeField] GameObject flotLampSpotLight;
        [SerializeField] GameObject heroEffectLight;    //直接代码内自调节数值

        private float jumpCostST{get{return HeroStatus.instance.jumpCostST;}}
        private float lampOnCostMP{get{return HeroStatus.instance.lampOnCostMP;}}
        private float dashCostST{get{return HeroStatus.instance.dashCostST;}} 
        private float runCostST{get{return HeroStatus.instance.runCostST;}}   

        public bool isGrounded, jumpBntPressed, isJumping, dashBntPressed, isMPRecover, isLampOn, isRunning; //TODO: 现方法overlapCircle，考虑之后要不要更换isGrounded();
        public bool isSpotLightOn, isPointLightOn;
        private Rigidbody2D rb{get{return GetComponent<Rigidbody2D>();}}
        private Animator anim{get{return GetComponent<Animator>();}}
        public TrailRenderer tr; //FIXME: 需要传入参数
        private float lastDash = -10f, dashTimeLeft; //FIXME:需要加入这两个变量的说明
        private float currentSpeed = 0;
        public float secondJumpCD;  //跳跃和二段跳之间的CD
        public int JumpCount; //measure how many jumps left
        public int direction {get{return HeroStatus.instance.direction;}set{HeroStatus.instance.direction = value;}} //这里选择把direction传回SO
        private Vector3 currentScale;
        private bool alive = true;

        #region Declare delegate variables
        public delegate void CharacterControl(); //创建HeroController的delegate
        private CharacterControl actionDelegateFuncs; //人物动作相关delegate（fixedupdate 调用）  //TODO:将这块以后加入事件系统
        #endregion
        void Start()
        {
            isLampOn = false;
            currentScale = this.transform.localScale;
            EventSystem.instance.changeLight += ChangeLightHandler; //sp
            // lamp.SetActive(false);
            flotLampPointLight.SetActive(false);
            heroEffectLight.GetComponent<Light2D>().intensity = .6f;
            
            //生成Layermask “Environment”
            Ground = LayerMask.GetMask("Environment");

            //将与人物动作相关的func加入actionDelegate中
            actionDelegateFuncs += Dash;
            actionDelegateFuncs += Jump;
            actionDelegateFuncs += Run;
            
        }

        private void Update()
        {
            secondJumpCD -= Time.deltaTime;
            
            //if Jump condition is satisfied
            isGrounded = Physics2D.OverlapCircle(GroundCheck.position, 0.2f, Ground); //TODO: 0.2f半径大小需要测试，如需调试会放在Character_SO中
            if (Input.GetButtonDown("Jump") && JumpCount > 0 && HeroStatus.instance.heroCurrentMP > jumpCostST) {jumpBntPressed = true;}

            if (Input.GetAxisRaw("Horizontal") > 0) {direction = 1;}
            if (Input.GetAxisRaw("Horizontal") < 0) {direction = -1;}
            //!Project中dir参数依旧不会等于0；
            //second jump 倒计时器

            //if dash condition is satisfied
            if (Input.GetKeyDown(KeyCode.LeftShift) && HeroStatus.instance.heroCurrentMP > dashCostST && Time.time >= (lastDash + HeroStatus.instance.dashCountDown))  
            {
                dashBntPressed = true;
                dashTimeLeft = HeroStatus.instance.dashDuration;
                lastDash = Time.time; 
                HeroStatus.instance.heroCurrentST -= dashCostST;
            }
            
            //if run condition is satisfied
            //TODO: FIXME!! RUN的主要实现方式，在update中检测，在fixedupdate中实现
            if (Input.GetKeyDown(KeyCode.RightShift) && isGrounded) {isRunning = true; currentSpeed = HeroStatus.instance.runSpeed;} //角色必须在地上才能跑步，按键按下时赋予runSpeed
            else {isRunning = false; currentSpeed = HeroStatus.instance.walkSpeed;}                                                  //角色在空中或是按键送开始回归walkSpeed

            if (alive)
            {
                Hurt();
                Die();
                LightUp();
            }
        }

        private void FixedUpdate() 
        {
            if (alive)
            {
                actionDelegateFuncs(); //actiondelegate此时包含Jump，dash，run
            }
        }

    /// <summary>
    /// 更换灯状态的事件订阅处理器
    /// </summary>
    public void ChangeLightHandler()
        {
            if (!isLampOn)                      //如果灯没开直接打开射灯模式
            {
                isLampOn = true;
                isSpotLightOn = true;
                flotLampSpotLight.SetActive(true);
                heroEffectLight.GetComponent<Light2D>().intensity = 0.5f;       //随着灯光调节人物自放光防止过曝
            }
            else                                //如果开着灯，先判断是否开了射灯，开了就换成点灯，点灯开了就全部关掉
            {
                if (isSpotLightOn && !isPointLightOn)                          //第二步切换到点光源
                {
                    isPointLightOn = true;
                    flotLampPointLight.SetActive(true);
                    isSpotLightOn = false;
                    flotLampSpotLight.SetActive(false);
                    return;                                 //直接返回
                }
                if (isPointLightOn && !isSpotLightOn)       //第三步全开
                {
                    isPointLightOn = true;
                    flotLampPointLight.SetActive(true);
                    isSpotLightOn = true;
                    flotLampSpotLight.SetActive(true);
                    return;                                 //直接返回
                }
                isLampOn = false;                           //全关
                isPointLightOn = false;
                flotLampPointLight.SetActive(false);
                isSpotLightOn = false;
                flotLampSpotLight.SetActive(false);
                heroEffectLight.GetComponent<Light2D>().intensity = 2.4f;       //随着灯光调节人物自放光防止过曝
            }
        }
        
    /// <summary>
    /// 所有关于Run相关的fixupdate
    /// </summary>
    private void Run()
    {
        Vector2 moveVelocity = Vector2.zero;
        anim.SetBool("isRun", false);           //TODO: anim 名称等待后续修改（我找不到char的anim。。。）

        if (Input.GetAxisRaw("Horizontal") < 0) //在此不能使用dir作为if条件，否则人物将无法停止移动
        {
            moveVelocity = Vector2.left;
            transform.localScale = new Vector3(currentScale.x * direction, currentScale.y, currentScale.z);
            if (!anim.GetBool("isJump"))
                anim.SetBool("isRun", true);
        }
        if (Input.GetAxisRaw("Horizontal") > 0)
        {
            moveVelocity = Vector2.right;
            transform.localScale = new Vector3(currentScale.x * direction, currentScale.y, currentScale.z);
            if (!anim.GetBool("isJump"))
                anim.SetBool("isRun", true);
        }
        
        //TODO:RUN animation接口
        if (isRunning) 
        {
            HeroStatus.instance.heroCurrentST -= runCostST * Time.deltaTime;
            //anim.SetBOOl("",true);
        }
        
        rb.position += moveVelocity * currentSpeed * Time.deltaTime;
    }
    
    /// <summary>
    /// 所有关于jump相关的fixupdate
    /// </summary>
    private void Jump()
    {
        anim.SetBool("isJump", false);
        if (isGrounded)                                         //在地上不会跳跃
        {
            JumpCount = HeroStatus.instance.maxJumpAllowed;
            isJumping = false;
            anim.SetBool("isJump", false);
        } else {isJumping = true;}                              //允许下落时跳跃
        if (jumpBntPressed && isGrounded)                       //一段跳
        {
            isJumping = true;
            rb.velocity = new Vector2(rb.velocity.x, HeroStatus.instance.jumpSpeed);
            JumpCount--;
            jumpBntPressed = false;
            anim.SetBool("isJump", true);
            secondJumpCD = HeroStatus.instance.secondJumpCD; //开始倒计时跳跃和二段跳之间的开始时间

            HeroStatus.instance.heroCurrentST -= jumpCostST;
        }
        else if (jumpBntPressed && isJumping && JumpCount > 0 && secondJumpCD < 0)  //二段跳
        {
            rb.velocity = new Vector2(rb.velocity.x, HeroStatus.instance.jumpSpeed);
            JumpCount--;
            jumpBntPressed = false;
            anim.SetBool("isJump", true);

            HeroStatus.instance.heroCurrentST -= jumpCostST;    //二段跳跃扣除st
        }
        jumpBntPressed = false;
    }

    /// <summary>
    /// 冲刺动作，shift按键触发
    /// </summary>
    private void Dash() 
    {
        if (dashBntPressed)
        {
            int tempDir = direction; //for later use
            if (dashTimeLeft <= 0)
            {   //dash结束后横向速度归零
                dashBntPressed = false;
                rb.velocity = new Vector2(0, rb.velocity.y); 
            }
            if (dashTimeLeft > 0) 
            {   //dash进行时

                // if (Input.GetAxisRaw("Horizontal") != 0)  //for later use,如果增加dir为零的参数这两行代码可以使人物idle->dash
                //     tempDir = direction;
                rb.velocity = new Vector2(HeroStatus.instance.dashSpeed * tempDir, rb.velocity.y);
                anim.SetBool("isJump", true);
                dashTimeLeft -= Time.deltaTime;
            }
        }
    }
    
    /// <summary>
    /// 开灯的动作，鼠标左键触发
    /// </summary>
    private void LightUp()
    {
        if (Input.GetAxis("Mouse ScrollWheel") != 0 && HeroStatus.instance.heroCurrentMP > lampOnCostMP)
        {
            anim.SetTrigger("attack");
            EventSystem.instance.ChangeLight();//sp
        }
    }
    
    /// <summary>
    /// 受伤相关
    /// </summary>
    private void Hurt()
    {
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            anim.SetTrigger("hurt");
            if (direction == 1)
                rb.AddForce(new Vector2(-5f, 1f), ForceMode2D.Impulse);
            else
                rb.AddForce(new Vector2(5f, 1f), ForceMode2D.Impulse);
        }
    }
    
    /// <summary>
    /// 死亡相关处理
    /// </summary>
    private void Die()
    {
        if (HeroStatus.instance.heroCurrentHP <= 0)
        {
            anim.SetTrigger("die");
            alive = false;
        }
    }

    /// <summary>
    /// 爬墙相关，现在弃用中
    /// </summary>
    private void Clamb()
    {

    }


    /// <summary>
    /// 重置状态， 弃用中
    /// </summary>
    private void Restart()
    {
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            anim.SetTrigger("idle");
            alive = true;
        }
  
    }
        
    //isgrounded Getter函数
    public bool IsGrounded() => isGrounded;
        
 }

