﻿//namespace DentedPixel{

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// 单例模式，处理所有的hero的状态
/// </summary>
public class HeroStatus : MonoBehaviour
{
    public static HeroStatus instance;
    public CharactorData_SO heroBData;
    public HeroHope_SO heroHopeData;
    public HeroObjects_SO heroOBJData;

    #region 基础变量的properties
    //[Header("基础属性相关")]
    #region 基础属性相关{get,set}
    public float heroMaxMP { get { return (heroBData != null ? heroBData.maxMP : 0); } set { heroBData.maxMP = value; }}
    public float heroCurrentMP { get { return (heroBData != null ? heroBData.currentMP : 0); } set { heroBData.currentMP = value; } }
    public float heroCurrentMaxMP { get { return (heroBData != null ? heroBData.currentMaxMP : 0); } set { heroBData.currentMaxMP = value; } }
    public float heroCurrentST { get { return (heroBData != null ? heroBData.currentST : 0); } set { heroBData.currentST = value; } } //因为ST动态上限为CurrentMP，MAXST不在另外赋值
    
    public float heroMaxHP{ get { return (heroBData != null ? heroBData.maxHP : 0); } set { heroBData.maxHP = value; }}
    public float heroCurrentHP{ get { return (heroBData != null ? heroBData.currentHP : 0); } set { heroBData.currentHP = value; }}
    public float heroCurrentMaxHP{ get { return (heroBData != null ? heroBData.currentMaxHP : 0); } set { heroBData.currentMaxHP = value; }}
    public float walkSpeed{ get { return (heroBData != null ? heroBData.walkSpeed : 0); } set { heroBData.walkSpeed = value; }}
    public float runSpeed{ get { return (heroBData != null ? heroBData.runSpeed : 0); } set { heroBData.runSpeed = value; }}
    public float jumpSpeed{ get { return (heroBData != null ? heroBData.jumpSpeed : 0); } set { heroBData.jumpSpeed = value; }}
    public float dashSpeed{ get { return (heroBData != null ? heroBData.dashSpeed : 0); } set { heroBData.dashSpeed = value; }}
    public int maxJumpAllowed{ get { return (heroBData != null ? heroBData.maxJumpAllowed : 0); } set { heroBData.maxJumpAllowed = value; }}
    public float dashCountDown{ get { return (heroBData != null ? heroBData.dashCountDown : 0); } set { heroBData.dashCountDown = value; }}
    public float dashDuration{ get {return (heroBData != null ? heroBData.dashDuration : 0); } set { heroBData.dashDuration = value; }}
    public float secondJumpCD{ get {return (heroBData != null ? heroBData.secondJumpCD : 0); } set { heroBData.secondJumpCD = value; }}

    public int direction { get {return (heroBData != null ? heroBData.direction : 0); } set { heroBData.direction = value;}}
    [HideInInspector]public float heroLowLimitMP;
    #endregion

    //[Header("希望值回复相关")]
    private float idleMPRecoverSpeed { get { return (heroHopeData != null ? heroHopeData.idleMPRecoverSpeed : 0); } set { heroHopeData.idleMPRecoverSpeed = value; }}
    private float movingMPRecoverSpeed{ get { return (heroHopeData != null ? heroHopeData.moveMPRecoverSpeed: 0); } set { heroHopeData.moveMPRecoverSpeed = value; }}
    private float idleSTRecoverSpeed { get { return (heroHopeData != null ? heroHopeData.idleSTRecoverSpeed : 0); } set { heroHopeData.idleSTRecoverSpeed = value; }}
    private float movingSTRecoverSpeed{ get { return (heroHopeData != null ? heroHopeData.moveSTRecoverSpeed: 0); } set { heroHopeData.moveSTRecoverSpeed = value; }}

    //[Header("希望值相关消耗")]
    #region 希望值相关消耗{get,set}
    public float runCostST{ get { return (heroHopeData != null ? heroHopeData.runCostST: 0); } set { heroHopeData.runCostST = value; }}
    public float jumpCostST{ get { return (heroHopeData != null ? heroHopeData.jumpCostST: 0); } set { heroHopeData.jumpCostST = value; }}
    public float dashCostST{ get { return (heroHopeData != null ? heroHopeData.dashCostST: 0); } set { heroHopeData.dashCostST= value; }}
    public float lampOnCostMP{ get { return (heroHopeData != null ? heroHopeData.lampCostMP: 0); } set { heroHopeData.lampCostMP = value; }}
    public float lampCostPerSec{ get { return (heroHopeData != null ? heroHopeData.lampOnMPCostPerSec: 0); } set { heroHopeData.lampOnMPCostPerSec = value; }}
    public float maxMPDrop{ get { return (heroHopeData != null ? heroHopeData.maxMPDrop: 0); } set { heroHopeData.maxMPDrop = value; }}
    public float secMPDrop { get { return (heroHopeData != null ? heroHopeData.secMaxMPDrop : 0); } set { heroHopeData.secMaxMPDrop = value; } }
    public float mpRecoverPoints{ get { return (heroHopeData != null ? heroHopeData.recoverMPBasicPerSec: 0); } set { heroHopeData.recoverMPBasicPerSec = value; }}
    public float stRecoverPoints{ get { return (heroHopeData != null ? heroHopeData.recoverSTBasicPersec: 0); } set { heroHopeData.recoverSTBasicPersec = value; }}
    public float Percent_heroLowLimitMP{ get { return (heroHopeData != null ? heroHopeData.lowestMaxMPFloorInPercent: 0); } set { heroHopeData.lowestMaxMPFloorInPercent = value; }}
    #endregion
    
    [Header("布尔值相关（测试用）")]
    public bool isLampOn;
    public bool isLightArea, isMPRecover;
    #endregion

    private void Awake()
    {
        #region 单例设置
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            if (instance != this)
            {
                Destroy(gameObject);
            }
        }
        DontDestroyOnLoad(gameObject);
        #endregion
    }

    private void Start()            //初始化
    {
        heroCurrentHP = heroMaxHP;
        heroCurrentMaxHP = heroMaxHP;
        heroCurrentMP = heroMaxMP;
        heroCurrentMaxMP = heroMaxMP;
        heroCurrentST = heroCurrentMP;
        heroLowLimitMP = heroMaxMP * Percent_heroLowLimitMP;
        isLampOn = false;
        isLightArea = false;
        isMPRecover = false;

        EventSystem.instance.changeLight += LightOnMPHandler;
        EventSystem.instance.enterArea += EnteringLightArea;
        EventSystem.instance.exitArea += ExitingLightArea;
    }

    private void Update()
    {
        MPnSTRecovering();
        StatusCeilAndFloor();

        //Debuging

        // Debug.Log("Current ST value: " + heroCurrentST); //由于st在hud中不会显示，此行用于监视st值
        // Debug.Log("Current MP value: " + heroCurrentMP);
    }

    /// <summary>
    /// 每秒自动回复mp的数
    /// </summary>
    private void MPnSTRecovering()
    {
        if ((Input.GetAxisRaw("Horizontal") == 0 || Input.GetAxisRaw("Vertical") == 0) 
                && !Input.GetButtonDown("Jump") && !Input.GetKeyDown(KeyCode.LeftShift)) //表示处于静止状态(并且不包含原地跳跃与冲刺状态)
        {
            heroCurrentMP += mpRecoverPoints * idleMPRecoverSpeed * Time.deltaTime;
            heroCurrentST += stRecoverPoints * idleSTRecoverSpeed * Time.deltaTime; //暂定于mp使用同种恢复方式，静止与运动分开
        }
        else
        {
            heroCurrentMP += mpRecoverPoints * movingMPRecoverSpeed * Time.deltaTime;
            if (!Input.GetKeyDown(KeyCode.RightShift))                                  //
                heroCurrentST += stRecoverPoints * movingSTRecoverSpeed * Time.deltaTime;
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            isMPRecover = true;
        }
    }

    /// <summary>
    /// 对状态属性限制上限和下限
    /// </summary>
    private void StatusCeilAndFloor()
    {
        //heroMP总上限相关
        if (heroCurrentMaxMP > heroMaxMP)
        {
            heroCurrentMaxMP = heroMaxMP;
        }
        else if (heroCurrentMaxMP < heroLowLimitMP)
        {
            heroCurrentMaxMP = heroLowLimitMP;
        }

        //hero HP 相关
        if (heroCurrentHP < 0)
        {
            heroCurrentHP = 0;
        }
        else if (heroCurrentHP > heroCurrentMaxHP)
        {
            heroCurrentHP = heroCurrentMaxHP;
        }

        //hero MP 相关
        if (heroCurrentMP < 0)
        {
            heroCurrentMP = 0;
        }
        else if (heroCurrentMP > heroCurrentMaxMP)
        {
            heroCurrentMP = heroCurrentMaxMP;
        }

        //hero ST 相关
        if (heroCurrentST < 0) heroCurrentST = 0;
        else if (heroCurrentST > heroCurrentMP) heroCurrentST = heroCurrentMaxMP; //保证自然恢复的CurrentST上限不会超过heroCurrentMP
    }

    //TODO: waiting for implements about maxMP drop n% by every m secs. Drop naturally with no interruption
    //private void NaturallyDropMPpersec(){}

    #region 火把控制相关
    private void EnteringLightArea(int _a)
    {
        isLightArea = true;
     }

    private void ExitingLightArea(int _a)
    {
        isLightArea = false;
    }
    #endregion

    #region 开关灯消耗相关
    /// <summary>
    /// 用于开灯扣除mp的协程， 由lightCostPerSec控制
    /// 已废除
    /// </summary>
    /// <returns>每秒扣除lightCostPerSec点数</returns>
    //IEnumerator LightMPCosmue()
    //{
    //    while (isLampOn)
    //    {
    //        heroCurrentMP -= lampCostPerSec;
    //        if (heroCurrentMP <= 1) //如果mp不够了直接关闭灯, 因为MP会一直回复，所以最后会在0~1之间的值里横跳
    //        {
    //            EventSystem.instance.ChangeLight();
    //            isLampOn = false;
    //            continue;
    //        }
    //        yield return new WaitForSeconds(1);
    //    }
    //}

    /// <summary>
    /// 灯光变动的事件响应的Handler，用于处理开灯后对于MP的相关操作
    /// </summary>
    private void LightOnMPHandler()
    {
        isLampOn = !isLampOn;
        if (isLampOn == true)
        {
            heroCurrentMP -= lampOnCostMP;      //开灯扣除MP
            StartCoroutine(StepDropMaxMP());    //开始每秒扣除MP上限
        }
        StopCoroutine(StepDropMaxMP());         //结束每秒扣除MP上限
        //StartCoroutine(LightMPCosmue()); //因为sprint 4 #83 而废除的
    }

    /// <summary>
    /// 阶段式扣除MP上限
    /// </summary> 
    /// <returns>如果不再光照区域，在SecMPDrop后返回查看是否在光照区域，不再则继续扣除</returns>
    IEnumerator StepDropMaxMP()
    {
        while (true)
        {
            if (!isLightArea)
            {
                heroCurrentMaxMP -= maxMPDrop;
                yield return new WaitForSeconds(secMPDrop);
            }
            yield return 0;
        }

    }
    #endregion

}
