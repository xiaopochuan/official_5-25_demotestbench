﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// MP回复区域，篝火相关
/// </summary>
public class SaveArea : MonoBehaviour
{
    [SerializeField] private bool isInSaveArea;
    [SerializeField] private float mpMaxRecoverPerSec;
    [SerializeField] private GameObject saveAreaUI;
    IEnumerator CheckKeyRecoverMP()
    {
        bool _isStartRecover = false;
        while (isInSaveArea)
        {
            if (HeroStatus.instance.isMPRecover) //按下E按键开始回复MP上限
            {
                Debug.Log("press recover");
                _isStartRecover = true;
            }
            while (isInSaveArea && _isStartRecover)
            {
                HeroStatus.instance.heroCurrentMaxMP += mpMaxRecoverPerSec;
                yield return new WaitForSeconds(1f);
            }
            yield return new WaitForEndOfFrame();
        }
    }

    private void EnterSaveAreaHandler()
    {
        LeanTween.scale(saveAreaUI, Vector3.one, 1f).setEase(LeanTweenType.easeInQuart);
        isInSaveArea = true;
        Debug.Log("按下E开始回复MP上限");
        StartCoroutine(CheckKeyRecoverMP());
    }

    private void ExitSaveAreaHandler()
    {
        LeanTween.scale(saveAreaUI, Vector3.zero, 1f).setEase(LeanTweenType.easeOutQuart); //UI 动画效果
        isInSaveArea = false;
        HeroStatus.instance.isMPRecover = false;
    }

    private void Start()
    {
        //找到并且默认关闭save area的UI
        saveAreaUI = GameObject.Find("SaveAreaUI");
        saveAreaUI.transform.localScale = Vector3.zero;
        EventSystem.instance.enterSaveArea += EnterSaveAreaHandler;
        EventSystem.instance.exitSaveArea += ExitSaveAreaHandler;
    }
}
