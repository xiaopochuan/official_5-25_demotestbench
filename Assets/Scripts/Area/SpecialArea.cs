﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(AreaTrigger))]
/// <summary>
/// [基本已弃用]三种不同区域作用与MP的class
/// </summary>
public class SpecialArea : MonoBehaviour
{
    [Header("黑暗区域相关设定")]
    [SerializeField] public float darkPoint;

    [SerializeField] public float darkSec;

    [Header("光明区域相关设定")]
    [SerializeField] public float lightPoint;

    [SerializeField] public float lightSec;

    [Header("普通区域相关设定")]
    [SerializeField] public float normalPoint;

    [SerializeField] public float normalSec;

    [SerializeField] public AreaType thisArea;

    [SerializeField] private int areaID;

    [SerializeField] private bool isDarkArea, isLightArea, isNormalArea;

    public enum AreaType
    {
        LightArea,
        DarkArea,
        NormalArea
    }
    /// <summary>
    /// 区域对于MP影响的协程
    /// </summary>
    /// <param name="_point">每n秒扣除的点数</param>
    /// <param name="_sec">多少秒扣一次</param>
    /// <param name="_areaType">当前区域的类型，string表示</param>
    /// <returns>等待_sec秒后执行一次</returns>
    IEnumerator AreaEffector(float _point, float _sec, string _areaType)
    {
        while (GetAreaBool(_areaType))
        {
            if (HeroStatus.instance.isLampOn)
            {
                HeroStatus.instance.heroCurrentMaxMP += _point;
            }
            yield return new WaitForSeconds(_sec);
        }
    }

    /// <summary>
    /// 处理hero进入area的事件handler
    /// </summary>
    /// <param name="_otherID">触发事件区域的ID</param>
    private void EnterAreaHandler(int _otherID)
    {
        if (areaID == _otherID)
        {
            switch (thisArea)
            {
                case AreaType.LightArea:
                    isLightArea = true;
                    LightAreaHandler();
                    break;
                case AreaType.DarkArea:
                    isDarkArea = true;
                    DarkAreaHandler();
                    break;
                case AreaType.NormalArea:
                    isNormalArea = true;
                    NormalAreaHandler();
                    break;
                default:
                    Debug.LogError("no detection of area type");
                    break;
            }
        }
    }

    /// <summary>
    /// 处理hero离开该区域的事件handler
    /// </summary>
    /// <param name="_otherID">区域ID</param>
    private void ExitAreaHandler(int _otherID)
    {
        if (areaID == _otherID)
        {
            switch (thisArea)
            {
                case AreaType.LightArea:
                    isLightArea = false;
                    break;
                case AreaType.DarkArea:
                    isDarkArea = false;
                    break;
                case AreaType.NormalArea:
                    isNormalArea = false;
                    break;
                default:
                    Debug.LogError("no detection of area type");
                    break;
            }
        }
    }

    /// <summary>
    /// 用于协程AreaEffector，判断时候离开了这个区域
    /// </summary>
    /// <param name="_areaType">区域的类型</param>
    /// <returns>返回一个bool，判断是否还呆在这个区域里</returns>
    private bool GetAreaBool(string _areaType)
    {
        switch (_areaType)
        {
            case "LightArea":
                return isLightArea;
            case "DarkArea":
                return isDarkArea;
            case "NormalArea":
                return isNormalArea;
            default:
                return false;
        }
    }

    private void Start()
    {
        areaID = this.GetInstanceID();
        EventSystem.instance.enterArea += EnterAreaHandler;
        EventSystem.instance.exitArea += ExitAreaHandler;
    }
    #region Handler 相关subrotine
    private void DarkAreaHandler()
    {
        StartCoroutine(AreaEffector(darkPoint, darkSec, "DarkArea"));
    }

    private void LightAreaHandler()
    {
        StartCoroutine(AreaEffector(lightPoint, lightSec, "LightArea"));
    }

    private void NormalAreaHandler()
    {
        StartCoroutine( AreaEffector(normalPoint, normalSec, "NormalArea"));
    }
    #endregion
}
