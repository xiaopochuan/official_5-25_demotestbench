var class_hero_hope___s_o =
[
    [ "dashCostMP", "class_hero_hope___s_o.html#ad798c2b7132e6844d0bdc8766bfc3024", null ],
    [ "idleMPRecoverSpeed", "class_hero_hope___s_o.html#a47ba79bdab3aa17c32dfda2f7a682986", null ],
    [ "jumpCostMP", "class_hero_hope___s_o.html#a89cb731e551bbb8575e3b8179e26ebb6", null ],
    [ "lampCostMP", "class_hero_hope___s_o.html#a1136f28e2f7132a6897cb29479147a23", null ],
    [ "lampOnMPCostPerSec", "class_hero_hope___s_o.html#a8f017f60673b85507cd734370253ae89", null ],
    [ "lowestMaxMPFloorInPercent", "class_hero_hope___s_o.html#ac7f95e2c1dd5c81075e3e232622ed1a5", null ],
    [ "maxMPDrop", "class_hero_hope___s_o.html#af524dd9f631906256b241eb11a473131", null ],
    [ "moveMPRecoverSpeed", "class_hero_hope___s_o.html#ad6e7eb7517a9b9660f1426bb2801e17e", null ],
    [ "recoverMPBasicPerSec", "class_hero_hope___s_o.html#a51018d24678785b686db2998fff9f6dc", null ],
    [ "secMaxMPDrop", "class_hero_hope___s_o.html#ab8822edfdc5a7728a5dd8ea15326e197", null ]
];