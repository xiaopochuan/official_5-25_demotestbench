﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Experimental.Rendering.Universal;
/// <summary>
/// [现阶段]当光照不和Hero绑定的时候用于控制光照跟随的class
/// [修改中] 全局的光照控制机制class
/// </summary>
public class LightManager : MonoBehaviour
{
    public static LightManager instance;
    [SerializeField] GameObject hero;
    [SerializeField] GameObject staff;
    [SerializeField] Light2D heroRingLight;
    // [SerializeField] Light2D heroStaffLight; //移动到FlotLamp 弃用

    private Vector3 heroRingLightOffset;
    private void Start()
    {
        heroRingLightOffset = heroRingLight.transform.position - hero.transform.position;
        // heroStaffLightOffset = heroStaffLight.transform.position - staff.transform.position; //移动到FlotLamp 弃用
    }
    private void Update()
    {
        LightPositionUpdate();
    }
    
    /// <summary>
    /// hero的两个光照，自发光的物品照射光的更新
    /// </summary>
    private void LightPositionUpdate()
    {
        heroRingLight.transform.position = hero.transform.position + heroRingLightOffset;
        // heroStaffLight.transform.position = staff.transform.position + heroStaffLightOffset; //移动到FlotLamp 弃用
    }
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
}
