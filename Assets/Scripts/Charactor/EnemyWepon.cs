using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 用于直接绑定到敌人攻击到hero的物体上的Tirgger脚本
/// </summary>
public class EnemyWepon : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("Hit Hero");
            EventSystem.instance.HeroBeenHit();
        }
    }
}
