var class_event_system =
[
    [ "ChangeLight", "class_event_system.html#a383f8842d107fdd383455e55f2824eaa", null ],
    [ "EnteringArea", "class_event_system.html#a5b1a3d10c9d55754fb876124ec7b3371", null ],
    [ "EnterSaveArea", "class_event_system.html#a4c094f65a0d3ebd41029e73288ffc350", null ],
    [ "ExitingArea", "class_event_system.html#a386343d91f82307d9820767dd26c7b61", null ],
    [ "ExitSaveArea", "class_event_system.html#a4a83b9d711172dd2cb464456db95ea1e", null ],
    [ "HeroBeenHit", "class_event_system.html#a718a68c107540be1b57ed8d3c1841e19", null ],
    [ "changeLight", "class_event_system.html#ac90163fcd1f22ea7946eaea5e55c29e7", null ],
    [ "enterArea", "class_event_system.html#a92a94b323c0b03d926b791203631b958", null ],
    [ "enterSaveArea", "class_event_system.html#a4c35861ff9453b15ee2c9739ace036e7", null ],
    [ "exitArea", "class_event_system.html#a4ba5b5da563fc793d1c5d38df5cea95d", null ],
    [ "heroBeenHit", "class_event_system.html#a86526cab3d4d85a5cf8e7b90640e1fab", null ]
];