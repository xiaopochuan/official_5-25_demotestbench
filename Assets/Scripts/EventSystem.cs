﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// 单例模式，全局的事件调用class
/// </summary>
public class EventSystem : MonoBehaviour
{
    public static EventSystem instance;

    public event Action<int> enterArea;
    public event Action<int> exitArea;
    public event Action changeLight;
    public event Action enterSaveArea, exitSaveArea;
    public event Action heroBeenHit, EnemyBeenHit;
    /// <summary>
    /// 订阅该事件需要通过IF obj.name
    /// </summary>
    public event Action<GameObject> pickUp, doorKeyDetect, movingGroundButtonPressed;

    //Debug 专用参数
    private int debugCounter;

    #region 对象外的所有public function

    public void MovingGroundButtonPressed(GameObject _obj)
    {
        if (movingGroundButtonPressed != null)
        {
            movingGroundButtonPressed(_obj);
        }
    }
    public void DoorKeyDetect(GameObject _obj)
    {
        if(doorKeyDetect != null)
        {
            doorKeyDetect(_obj);
        }
    }
    public void PickUp(GameObject _obj)
    {
        if (pickUp != null)
        {
            pickUp(_obj);
        }
    }
    public void HeroBeenHit()
    {
        if (heroBeenHit != null)
        {
            // debugCounter++;
            // Debug.Log(string.Format("Counter = {0}",debugCounter));
            heroBeenHit();
        }
    }
    public void ExitSaveArea()
    {
        if (exitSaveArea != null)
        {
            exitSaveArea();
        }
    }
    public void EnterSaveArea()
    {
        if (enterSaveArea != null)
        {
            enterSaveArea();
        }
    }
    public void EnteringArea(int _id)
    {
        if (enterArea != null)
        {
            enterArea(_id);
        }
    }

    public void ExitingArea(int _id)
    {
        if (exitArea != null)
        {
            exitArea(_id);
        }
    }

    public void ChangeLight()
    {
        if (changeLight != null)
        {
            changeLight();
        }
    }
    #endregion

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            if (instance != this)
            {
                Destroy(gameObject);
            }
        }
        DontDestroyOnLoad(gameObject);
    }
}
