var indexSectionsWithContent =
{
  0: "acdefghijlmnoprstuvwy",
  1: "acefhlsuv",
  2: "acefhlsuv",
  3: "cehopsu",
  4: "cdefghijlmnprstuvwy",
  5: "as",
  6: "adilnsw",
  7: "adhjlmps",
  8: "ceh"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "properties",
  8: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Properties",
  8: "Events"
};

