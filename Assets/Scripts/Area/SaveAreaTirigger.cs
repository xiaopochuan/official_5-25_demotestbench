﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// 篝火区域碰撞体脚本
/// </summary>
public class SaveAreaTirigger : MonoBehaviour
{

    // 如果另一个碰撞器 2D 进入了触发器，则调用 OnTriggerEnter2D (仅限 2D 物理)
    private void OnTriggerEnter2D(Collider2D collision)
    {
        EventSystem.instance.EnterSaveArea();
    }

    // 如果另一个碰撞器 2D 停止接触触发器，则调用 OnTriggerExit2D (仅限 2D 物理)
    private void OnTriggerExit2D(Collider2D collision)
    {
        EventSystem.instance.ExitSaveArea();
    }



}
