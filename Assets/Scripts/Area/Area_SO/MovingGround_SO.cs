using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New MovingGround type Data", menuName = "Object Data /MovingGround Data")]
public class MovingGround_SO : ScriptableObject
{
    [Header("基础属性")]
    public Sprite groundSprite;
    public Color groundColor;
    public Sprite buttonSprite;
    public Color buttonColor;
    public string str_OnButtonPressed;

}
