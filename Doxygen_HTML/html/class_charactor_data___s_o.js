var class_charactor_data___s_o =
[
    [ "currentHP", "class_charactor_data___s_o.html#a87dd0ea9f4b7ec24e9548ac07c0debea", null ],
    [ "currentMaxHP", "class_charactor_data___s_o.html#a65dec99f658c89d9ee2af95ea332d823", null ],
    [ "currentMaxMP", "class_charactor_data___s_o.html#a952a5af8cd8ad00ccfb32ee364d0ca72", null ],
    [ "currentMP", "class_charactor_data___s_o.html#a399791e7970d65a34b7508ed5ab50e68", null ],
    [ "dashCountDown", "class_charactor_data___s_o.html#a710ef23b6b816fd484bafa2daf5ca4d6", null ],
    [ "dashDuration", "class_charactor_data___s_o.html#a6bae5e67dbc80834aca8f6f9646a6b6e", null ],
    [ "dashSpeed", "class_charactor_data___s_o.html#a53b473bc42cd5bcdc62ec101fec1b670", null ],
    [ "direction", "class_charactor_data___s_o.html#a1dc04fd9ffd8e3a0729561b65be293c7", null ],
    [ "e_awakeMP", "class_charactor_data___s_o.html#a1d52eec8a16f2345e5e89b55fab36855", null ],
    [ "e_recoverMP", "class_charactor_data___s_o.html#ad3e65440ef126418973c11f11518a5f5", null ],
    [ "jumpSpeed", "class_charactor_data___s_o.html#a9ed60ea1228e882b966ae0b374dd403b", null ],
    [ "maxHP", "class_charactor_data___s_o.html#a9355311140117b246118b8c5e163af4e", null ],
    [ "maxJumpAllowed", "class_charactor_data___s_o.html#a5ea624453bbc3291565959a3afff778e", null ],
    [ "maxMP", "class_charactor_data___s_o.html#a86922fb5fa922e04cbc9d35c13aa2d08", null ],
    [ "moveSpeed", "class_charactor_data___s_o.html#a5fc2912cb1e418f062fdf61711d13d2b", null ],
    [ "secondJumpCD", "class_charactor_data___s_o.html#a68974699330681251bf7f6be2bd12246", null ],
    [ "yDiffTrack", "class_charactor_data___s_o.html#a96292652835d1b0c5174f3d962793fc4", null ]
];