﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// 光照区域区域碰撞检测
/// </summary>
public class AreaTrigger : MonoBehaviour
{
    [SerializeField] private int areaID; 
    private void Start()
    {
        areaID = this.GetComponent<SpecialArea>().GetInstanceID();//需要从对应区域获取的ID，不同component的id也不同
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        EventSystem.instance.EnteringArea(areaID);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        EventSystem.instance.ExitingArea(areaID);
    }



}
