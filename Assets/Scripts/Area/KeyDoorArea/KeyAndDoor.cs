using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyAndDoor : MonoBehaviour
{
    public GameObject keyOBJ;
    public GameObject doorOBJ;
    public GameObject doorPosRef;
    public KeyAndDoor_SO baseData;
    public Vector3 finalDestination;
    public float runTime;
    private float keyID{get{return keyOBJ.GetInstanceID();}}
    private float doorID{get{return doorOBJ.transform.GetChild(0).gameObject.GetInstanceID();}}
    private SpriteRenderer keySprite{get{return keyOBJ.GetComponent<SpriteRenderer>();}}
    private SpriteRenderer doorSprite{get{return doorOBJ.GetComponent<SpriteRenderer>();}}
    public bool unlockPressed;


    private void OnDrawGizmosSelected() 
    {
        //一个钥匙对应一张门
        if(keyOBJ!=null)Gizmos.DrawLine(keyOBJ.transform.position, doorOBJ.transform.position);

        //门移动相关
        Vector3 doorInitPosition = doorPosRef.transform.position;
        Gizmos.DrawWireCube(doorInitPosition+finalDestination, doorOBJ.transform.localScale);
    }

    void Start()
    {
        //赋值颜色和属性
        keySprite.sprite = baseData.keySprite;
        keySprite.color = baseData.keyColor;
        doorSprite.sprite = baseData.doorSprite;
        doorSprite.color = baseData.doorColor;

        //Event 订阅
        EventSystem.instance.doorKeyDetect +=  DoorUnlockHandler;
        EventSystem.instance.pickUp += KeyPickUpHandler;
    }

    private void KeyPickUpHandler(GameObject _objIn)
    {
        if(_objIn.GetInstanceID() != keyID ) return;

        if (_objIn.name == "Key")
        {
            HeroStatus.instance.heroOBJData.heroKeyNum++;
            Debug.Log(baseData.str_onKeyPick);

            //TODO: 钥匙动画效果在此之后实现
            Destroy(keyOBJ);
        }
    }

    private void DoorUnlockHandler(GameObject _objIn)
    {
        if(_objIn.GetInstanceID() != doorID) return;
            
        if (Input.GetKeyDown(KeyCode.E) && HeroStatus.instance.heroOBJData.heroKeyNum > 0)    //按下e按键判断
        {
            Debug.Log(baseData.str_OnDoorUnlock);
            LeanTween.move(doorOBJ,doorPosRef.transform.position+ finalDestination,runTime);
            HeroStatus.instance.heroOBJData.heroKeyNum--;
        }
    }   

    private void Update() 
    {
        unlockPressed =  Input.GetKey(KeyCode.E) ? true : false;
    }
}
