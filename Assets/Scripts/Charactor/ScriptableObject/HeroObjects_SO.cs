using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Hero Object Data", menuName = "Character Status /Hero Object Data")]
public class HeroObjects_SO : ScriptableObject
{
    [Tooltip("Hero目前持有的钥匙数量")]public int  heroKeyNum;
}
