using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New KeyAndDoor type Data", menuName = "Object Data /KeyAndDoor Data")]

public class KeyAndDoor_SO : ScriptableObject
{
    [Header("基础属性")]
    [Tooltip("门的贴图")]public Sprite doorSprite;
    [Tooltip("门的颜色")]public Color doorColor;
    [Tooltip("钥匙的贴图")]public Sprite keySprite;
    [Tooltip("钥匙的颜色")]public Color keyColor;
    [Tooltip("捡起钥匙时说的话")]public string str_onKeyPick;
    [Tooltip("解锁门时说的话")]public string str_OnDoorUnlock;

    // [Header("门相关")]
    // [Tooltip("门移动的总时间")]public float doorMoveTime;
    // [Tooltip("门最后的移动位置")]public Vector3 doorFinalDestination;
    // public Dir   doorDir;

    // [HideInInspector]public enum Dir
    // {
    //     Up,Down,Left,Right
    // }
}
