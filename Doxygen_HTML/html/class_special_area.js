var class_special_area =
[
    [ "AreaType", "class_special_area.html#ad057ca6b08a7f48914e3ebee74c1c90a", [
      [ "LightArea", "class_special_area.html#ad057ca6b08a7f48914e3ebee74c1c90aaa202e1797c3eb4e64786edbf4de7d48a", null ],
      [ "DarkArea", "class_special_area.html#ad057ca6b08a7f48914e3ebee74c1c90aa554fa48ccef9b8b2fd394e8c8c42ffed", null ],
      [ "NormalArea", "class_special_area.html#ad057ca6b08a7f48914e3ebee74c1c90aa1d4ee925e4adc90366d55037cc0f35d9", null ]
    ] ],
    [ "darkPoint", "class_special_area.html#aa66c3db7e43183961d5aa19fa8368be6", null ],
    [ "darkSec", "class_special_area.html#a7c03cff8e688c3e2cd03d97764cd4ed2", null ],
    [ "lightPoint", "class_special_area.html#aff3467326d9fa54fc92e581fedec0497", null ],
    [ "lightSec", "class_special_area.html#a009fc5b8678c426a1346d0dd13f9dd7b", null ],
    [ "normalPoint", "class_special_area.html#ae0004446649a25e104fa24cf20709c21", null ],
    [ "normalSec", "class_special_area.html#abd5d4f47e317cb59e6757d8be4017aaa", null ],
    [ "thisArea", "class_special_area.html#aeef2ed85f9c5f3bf0484720a122c46bd", null ]
];