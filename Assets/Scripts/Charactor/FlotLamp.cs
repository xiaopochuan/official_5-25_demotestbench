using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

//BUG:不知道为什么一开始FlotLamp是反的，但是后面正常了，怪起来了
[RequireComponent(typeof(Rigidbody2D))]
/// <summary>
/// 提灯相关所有处理的Class，继承自FSM_test
/// </summary>
public class FlotLamp : FSM_test
{
    //TODO: 粒子系统试用
    //TODO: 射灯与被照射物体的射线检测（模拟被光照到） 
    
    [Header("FlotLamp 特有属性")]
    [Tooltip("子物体，储存animator和FlotLamp图片的")]public GameObject spriteChild;
    [Tooltip("子物体，调用Spotlight")]public GameObject spotLightObj;
    [Tooltip("SO物体， 用于调用FlotLamp所有的基础属性")] public FlotLamp_SO flotBData;
    private float followSpeed{get{return flotBData!=null?flotBData.followSpeed:0;}set{flotBData.followSpeed=value;}}
    private float dirTransTime{get{return flotBData!=null?flotBData.dirTransTime:0;}set{flotBData.dirTransTime=value;}}
    private Vector2 yFloatSin{get{return flotBData!=null?flotBData.yFloatSin:Vector2.zero;}set{flotBData.yFloatSin=value;}}
    private Vector2 spotLightAngle{get{return flotBData!=null?flotBData.spotLightAngle:Vector2.zero;}set{flotBData.spotLightAngle=value;}}
    private Rigidbody2D rb{get{return GetComponent<Rigidbody2D>();}}
    private float ySinMagni{get{return yFloatSin.x;}}
    private float ySinFrequn{get{return yFloatSin.y;}}
    private float yRadUp{get{return spotLightAngle.x * (Mathf.PI / 180);}}
    private float yRadDown{get{return spotLightAngle.y * (Mathf.PI / 180);}}
    private int heroDir {get {return HeroStatus.instance.direction;}}
    private int unSignDir{get{return heroDir < 0 ? 1 : 0;}}
    private float initDis;
    private Vector3 initDir, stepTargetPos, tempVec;
    private Quaternion finalRotation;
    private int lastHeroDir;
    private float dirTimeCounter;

    //------------------DEV---------------------
    private Light2D spotLightComp{get{return spotLightObj.GetComponent<Light2D>();}}
    private PolygonCollider2D spotLightRangeColid{get{return spotLightObj.GetComponentInChildren<PolygonCollider2D>();}}
    private float initIntensity, initOutRadius;
    private FlictStage currentStage;
    private  IEnumerator flicCorotine;
    //------------------DEV---------------------
    
    public override void Start()
    {
        base.Start();
        initDis = heroDistance;
        initDir = (this.transform.position - hero.transform.position).normalized;
        lastHeroDir = heroDir;
        initIntensity = spotLightComp.intensity;
        initOutRadius = spotLightComp.pointLightOuterRadius;
        spotLightObj.SetActive(false);
    }

    public override void Update()
    {
        base.Update();
        TransDir();
        SpotLightMouseControll();
        SpotLightReactTOMPST();
        stepTargetPos = hero.transform.position + initDir * initDis;
    }

    private void FixedUpdate() 
    {
        rb.transform.position = Vector3.Lerp(transform.position, stepTargetPos, followSpeed * Time.deltaTime);
        //FIXME: 运用全局变量完成修改rotation的lerp
        finalRotation = Quaternion.Euler(0, 180 * unSignDir, -90);
        spotLightObj.transform.rotation = Quaternion.SlerpUnclamped(spotLightObj.transform.rotation, finalRotation, Time.deltaTime);
    }

    /// <summary>
    /// 辅助线绘制
    /// </summary>
    private void OnDrawGizmosSelected() 
    {
        //角度辅助线，不会随人物旋转
        float _length = 10;
        Vector3 startPoint = spotLightObj.transform.position;
        Vector2 _upAngle =  new Vector2( _length * Mathf.Cos(yRadUp) + startPoint.x, _length * Mathf.Sin(yRadUp) + startPoint.y);
        Vector2 _downAngle = new Vector2(_length * Mathf.Cos(yRadDown) + startPoint.x, _length * Mathf.Sin(yRadDown)+startPoint.y);
        Gizmos.DrawLine(startPoint, _upAngle);
        Gizmos.DrawLine(startPoint, _downAngle);

        //鼠标链接辅助线
        Vector3 zeroZ = Vector3.right + Vector3.up;                
        Vector3 _cameraThisPos = spotLightObj.transform.position;               //treat as point not vector 
        Vector3 _mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);//treat as point not vector 
        _cameraThisPos.Scale(zeroZ); //对于2d游戏减去深度
        _mousePos.Scale(zeroZ);
        Gizmos.DrawLine(_mousePos, _cameraThisPos);

        Vector3 _angleBaseLine = spotLightObj.transform.position + new Vector3(heroDir, 0, 0);
        Gizmos.DrawLine(_cameraThisPos, _angleBaseLine);
    }

    #region  所有和Flotmap具体与MP和ST交互的Funcitons
    /// <summary>
    /// 根据当前Hero的MP和ST值做出相应的修改
    /// </summary>
    private void SpotLightReactTOMPST()
    {
        SpotLightFlicControll();
        SpotLightRangeAndIntense();
    }

    /// <summary>
    /// 设置Collider通过MP与ST，MP与maxMP的比值确认光照范围的大小和强度
    /// </summary>
    private void SpotLightRangeAndIntense()
    {
        //CHECKME: 灯随着ST/maxMP值缩小范围的效果

        float radiusRatio = HeroStatus.instance.heroCurrentST / HeroStatus.instance.heroCurrentMP; //rangeRatio <= 1
        float intensRatio = HeroStatus.instance.heroCurrentMP / HeroStatus.instance.heroCurrentMaxMP; //rangeRatio <= 1
        //float rangeRatio = 1;
        //float intensRatio = 1;

        //CHECKME: 光照collider 修改
        float newRad = ((spotLightComp.pointLightInnerAngle / 2 + spotLightComp.pointLightOuterAngle /2)
                         /2 * (Mathf.PI / 180));//求均后再除二（中间分开）
        float lightRange = flotBData.maxLightRange * radiusRatio;                                
        Vector2[] newBound = new Vector2[3]
        {
            new Vector2(lightRange, Mathf.Tan(newRad) * lightRange),
            Vector2.zero,            //第二个是射灯中心点
            new Vector2(lightRange, -Mathf.Tan(newRad) * lightRange)
        };
        spotLightRangeColid.SetPath(0,newBound);

        //CHECKME: 实际光照范围与lightRange的参数转换
        spotLightComp.pointLightOuterRadius = radiusRatio * initOutRadius;
        //CHECKME: 灯随着mp/maxMP值减弱起强度的效果
        spotLightComp.intensity = intensRatio * initIntensity;
    }

    /// <summary>
    /// 通过调用协程的方式控制灯光状态从而控制灯光闪烁
    /// </summary>
    private void SpotLightFlicControll()
    {        
        //CHECKME: 灯随着ST/MP值闪烁的效果
        foreach (var stage in flotBData.flicStages)
        {   //TODO:CHECKME 不确定是否需要的是st，请查证
            if (HeroStatus.instance.heroCurrentST <= stage._rangeMap.y && HeroStatus.instance.heroCurrentST > stage._rangeMap.x) //NOTE: 位于当前区间里
            {
                if (currentStage != stage)
                {
                    currentStage = stage;
                    Debug.Log(stage._stageName);
                    // StopCoroutine("SpotFlicing");   //停止当前运行的闪烁模式
                    // StopAllCoroutines();
                    // StartCoroutine(SpotFlicing(stage._flicGroupFreqInSec, stage._flicCloseDuration, stage._flicDuration,
                    // stage._flicCountForAGroup,stage._Rand, stage._rangeMap));  //开始新的闪烁模式
                    if(flicCorotine != null) StopCoroutine(flicCorotine);
                    flicCorotine = SpotFlicing(stage._flicGroupFreqInSec, stage._flicCloseDuration, stage._flicDuration,stage._flicCountForAGroup,stage._Rand, stage._rangeMap);
                    StartCoroutine(flicCorotine);
                    return;
                }
                return;//找到区间马上return
            }
        }
        //还原常亮状态
        Debug.Log("不闪了");
        currentStage = null;
        spotLightComp.enabled = true;
        if(flicCorotine != null) StopCoroutine(flicCorotine);   //停止闪烁模式 
    }

    /// <summary>
    /// 协程调用，用于生存闪烁的效果，只能通过StopCoroutine停止
    /// </summary>
    /// <param name="_timeFlicFreqInGroup">每次【闪】之间的间隔时间</param>
    /// <param name="_timeFlicOffDuration">每次【闪烁】之间的间隔时长</param>
    /// <param name="_timeFlicOnDuration">每次【闪烁】的时长</param>
    /// <param name="_flicTimes">每次【闪】时闪烁的次数</param>
    /// <param name="_rand">是否开启【闪烁】间隔随机</param>
    /// <param name="_range">【闪烁】间隔随机的范围 in sec</param>
    /// <returns></returns>
    IEnumerator SpotFlicing(float _timeFlicFreqInGroup, float _timeFlicOffDuration, float _timeFlicOnDuration, int _flicTimes, int _rand, Vector2 _range)
    {
        while (true) //在没有被Stop之前会一直调用下去
        {
            // Debug.Log("Start Group Flic");
            float randTime = _rand * UnityEngine.Random.Range(_range.x, _range.y);
            for (int i = 0; i < _flicTimes; i++)
            {
                //Note: Active调用 二选一
                // spotLightObj.SetActive(false);                      //首先关闭
                // // Debug.Log(string.Format("Wait for {0}", _timeFlicOffDuration + _rand * UnityEngine.Random.Range(_range.x, _range.y)));
                // yield return new WaitForSeconds(_timeFlicOffDuration);//等待,同时调用随机数
                // // Debug.Log("Return"); 
                // spotLightObj.SetActive(true);                       //然后再开启
                // // Debug.Log(string.Format("Wait for {0}", _timeFlicOnDuration));
                // yield return new WaitForSeconds(_timeFlicOnDuration);

                //Note: 强度调用 二选一
                float currentIntensity = spotLightComp.intensity;    
                spotLightComp.intensity =  currentIntensity * 0.2f; //将闪烁强度调低0.5
                yield return new WaitForSeconds(_timeFlicOffDuration);//等待,同时调用随机数
                spotLightComp.intensity = currentIntensity;
                yield return new WaitForSeconds(_timeFlicOnDuration);            
            }
            // Debug.Log(string.Format("Wait for {0}", _timeFlicFreqInGroup));
            yield return new WaitForSeconds(_timeFlicFreqInGroup + randTime);         //等待下一个turn
        }
    }
    #endregion

    /// <summary> 
    /// 在disTransTime时间里给反应是否flip y轴，有个较为流畅的transit
    /// </summary>
    private void TransDir()
    {
        dirTimeCounter += Time.deltaTime;                   //第一版的基础思路，并不是特别好
        lastHeroDir = heroDir;
        if(dirTimeCounter >= dirTransTime)                  //有disTransTime的时间里变化暂时不会改变位置
        {
            dirTimeCounter = 0;
            initDir.Set(- lastHeroDir * Mathf.Abs(initDir.x), initDir.y, initDir.z);//经验方程....有优化空间
            //  =  Quaternion.Euler(0,180 * unSignDir, -90);         //翻转灯光
            float tempDir = unSignDir;      
        }
    }

    //CHECKME: 鼠标控制射灯方向
    /// <summary>
    /// 鼠标控制Light范围
    /// </summary>
    private void SpotLightMouseControll()
    {
        if (Input.GetKey(KeyCode.Mouse1)) //按住鼠标右键
        {
            //NOTE: 计算角度
            Vector3 zeroZ = Vector3.right + Vector3.up;                
            Vector3 _cameraThisPos = spotLightObj.transform.position;               //treat as point not vector 
            Vector3 _mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);//treat as point not vector
            _cameraThisPos.Scale(zeroZ); //对于2d游戏减去深度
            _mousePos.Scale(zeroZ);
            Vector3 _angleBaseLine =  spotLightObj.transform.position + new   Vector3(1, 0, 0);
            // Debug.Log(_mousePos);
            Vector3 tempVessel = new Vector3(_mousePos.x - _angleBaseLine.x,  _mousePos.y - _angleBaseLine.y); //方便把两个float整合成一个vector了
            float tempRad = Mathf.Atan2(tempVessel.y, heroDir * tempVessel.x); 
            float _addAngle  = tempRad / Mathf.PI * 180;
            

            //NOTE: 抑制输入输出大小
            // if (_addAngle > 100f) _addAngle = 100f;
            // if (_addAngle < -100f)_addAngle = -100f;

            //NOTE: 根据角度旋转spotlight
            float _finalAddAngle = -_addAngle.Remap(-180f,180f,spotLightAngle.x,spotLightAngle.y);
            // Debug.Log(_finalAddAngle);
            spotLightObj.transform.rotation =  Quaternion.Euler(0,180 * unSignDir, -90 + _finalAddAngle);  
        }
    }

    //TODO：射灯模组和碰撞体制作



    //CHECKME: 沿着y轴上下浮动效果
    /// <summary>
    /// 沿着y轴上下浮动效果
    /// </summary>
    private void SpriteHovering()
    {
        tempVec = Vector3.zero;         //使用的是localscale所以直接用v3.zero作为初始值
        float tempRand = UnityEngine.Random.Range(1,2); //感觉这个rand好像没用的样子
        tempVec.y = Mathf.Sin(Time.fixedTime * Mathf.PI * ySinFrequn * tempRand) * ySinMagni; 
        spriteChild.transform.localPosition = tempVec;
    }
    

    public override void StateIdle()
    {
        SpriteHovering();
        
    }

    public override void StateWalk()
    {

    }
}

public static class ExtensionMethods 
{
     /// <summary>
    /// 有点坑，只能算两者都是正负的
    /// </summary>
    /// <param name="inputValue">输入值，调用时直接在float后面.就行</param>
    /// <param name="realFrom">真实数据，开始范围</param>
    /// <param name="realTo">真实数据，结束范围</param>
    /// <param name="fakeFrom">虚构的数据，开始范围</param>
    /// <param name="fakeFrom">虚构数据，结束范围</param>
    /// <returns>返回一个虚构数据范围里输入值与真实数据范围的比例对应的数字，如2.Remap(1,3,0,10) == 5</returns>
     public static float Remap (this float inputValue, float realFrom, float realTo, float fakeFrom, float fakeTo)
      {return (inputValue - realFrom) / (realTo - realFrom) * (fakeTo - fakeFrom) + fakeFrom;}
  
}